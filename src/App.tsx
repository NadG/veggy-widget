import React, { Component } from 'react';
import Header from 'components/Atoms/Header/Header';
import Footer from 'components/Atoms/Footer/Footer';
import Main from 'components/Pages/Main/Main';
import a from 'App.module.scss';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className={a.main}>
          <Header />
          <Main />
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
