import React from 'react';
//import React, { useEffect, useRef, useState } from 'react';
import { Formik } from 'formik';
//import { Field, FieldArray, Form, Formik } from 'formik';
import Select from 'components/Atoms/Select/Select';

const RecipeForm: React.FC = () => {
  return (
    <div>
      <h1>New recipe</h1>
      <br />
      <Formik
        initialValues={{
          recipeName: '',
          mainIngredient: ['tomato', 'zucchini', 'orange'],
        }}
        onSubmit={values => {
          console.log(values);
        }}
        render={({ values }) => (
          <Select name="mainIngredient" data={values.mainIngredient} />
        )}
      />
    </div>
  );
};
export default RecipeForm;
