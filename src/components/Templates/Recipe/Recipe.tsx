import React, { useEffect, useRef, useState } from 'react';
import rs from 'components/Templates/Recipe/recipe.module.scss';
import classnames from 'classnames';
import { animated, useSpring, config } from 'react-spring';
import { LazyImage } from 'react-lazy-images';
import List from 'components/Atoms/List/List';
import Subtitle from 'components/Atoms/Subtitle/Subtitle';
import { ReactComponent as Go } from 'assets/icons/misc/next.svg';
import RecipeHeader from 'components/Molecules/RecipeHeader/RecipeHeader';
import RecipeGenerics from 'components/Molecules/RecipeGenerics/RecipeGenerics';
import Flex from 'components/Atoms/Flex/Flex';
import BorderedBox from 'components/Atoms/BorderedBox/BorderedBox';
import Anchor from 'components/Atoms/Anchor/Anchor';
import RecipeIngredientsSection from 'components/Molecules/RecipeIngredientsSection/RecipeIngredientsSection';

interface RecipeProps {
  image: string;
  alt: string;
  title: string;
  difficulty: string;
  time: string;
  isSweet?: boolean;
  isVegetarian?: boolean;
  isVegan?: boolean;
  hasMeat?: boolean;
  hasFish: boolean;
  allergens: string[];
  ingredients: [{ name: string; quantity: 'string' }];
  instructions: string[];
  link: string;
}

const Recipe: React.FC<RecipeProps> = ({ ...props }) => {
  const [expandCard, setExpandCard] = useState<boolean>(false);
  const [hover, setHover] = useState<boolean>(false);
  const [cardHeight, setCardHeight] = useState<number>(300);
  const hoverState = useSpring({
    transform: hover ? ' scale(1.03)' : 'scale(1)',
    config: config.gentle,
  });
  const heightState = useSpring({
    height: expandCard ? `${cardHeight}px` : '300px',
    config: config.gentle,
  });
  const el: any = useRef(null);
  let generics = {
    vegetarian: props.isVegetarian && !props.isVegan && !props.isSweet,
    vegan: props.isVegan || (props.isVegetarian && props.isVegan),
    sweet: props.isSweet || (props.isSweet && props.isVegetarian),
    meat: props.hasMeat,
    fish: props.hasFish,
  };

  useEffect(() => {
    let elHeight: any = el.current.offsetHeight;
    elHeight > 300 ? setCardHeight(elHeight) : setCardHeight(cardHeight);
  }, [expandCard, cardHeight]);

  return (
    <animated.div
      style={{ ...hoverState, ...heightState }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <div
        className={classnames(rs.card, {
          [rs.card_expanded]: expandCard,
        })}
        onClick={() => setExpandCard(!expandCard)}
        ref={el}
      >
        <article
          className={classnames(rs.article, {
            [rs.generals]: !expandCard,
            [rs.details]: expandCard,
          })}
        >
          <LazyImage
            src={props.image}
            alt={props.alt}
            debounceDurationMs={500}
            placeholder={({ imageProps, ref }) => (
              <img
                ref={ref}
                src="./../assets/images/empty_dish.jpg"
                height="300"
                width="320"
                alt={imageProps.alt}
              />
            )}
            actual={({ imageProps }) => (
              <img
                alt="img"
                {...imageProps}
                className={classnames(rs.img, rs.animated, rs.fadeIn)}
              />
            )}
          />
          <RecipeHeader list={props.allergens} title={props.title} />
          {expandCard ? (
            <>
              <div className={rs.ingredients_container}>
                <RecipeIngredientsSection ingredients={props.ingredients} />
              </div>
              <div className={rs.instructions_container}>
                <Subtitle words="Preparation" />
                <Flex column>
                  <List collection={props.instructions} />
                  <BorderedBox>
                    {props.link.length > 0 ? (
                      <Flex column>
                        <Anchor
                          link={props.link}
                          linkName="Go to the website of the recipe "
                        >
                          <Go className={rs.arrow} />
                        </Anchor>
                        <p className={rs.infobox}>
                          Most of the recipe links provided are just for
                          reference
                        </p>
                      </Flex>
                    ) : (
                      <p className={rs.infobox}>
                        Coming soon: link for reference
                      </p>
                    )}
                  </BorderedBox>
                </Flex>
              </div>
            </>
          ) : (
            <RecipeGenerics time={props.time} generics={generics} />
          )}
        </article>
      </div>
    </animated.div>
  );
};
export default Recipe;
