import React from 'react';
import month_style from 'components/Molecules/Month/Month.module.scss';
import CarouselList from 'components/Molecules/CarouselList/CarouselList';

interface MonthProps {
  id: number;
  key: number;
  month: string;
  fruits: string[];
  vegetables: string[];
}

const Month: React.FC<MonthProps> = ({ month, fruits, vegetables }) => {
  return (
    <div className={month_style.month_container}>
      <header>
        <h1 className={month_style.month_name}>{month}</h1>
      </header>
      <div role="main">
        <CarouselList type="fruits" list={fruits} />
        <CarouselList type="veggies" list={vegetables} />
      </div>
    </div>
  );
};

export default Month;
