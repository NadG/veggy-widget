import React from 'react';
import list_style from 'components/Molecules/CarouselList/CarouselList.module.scss';
import CarouselItem from 'components/Molecules/Item/CarouselItem';

interface ListProps {
  list: string[];
  type: 'fruits' | 'veggies';
}

const CarouselList: React.FC<ListProps> = ({ list, type }) => {
  let item = list.map((veg: string, i: number) => (
    <CarouselItem key={i} item={veg} />
  ));

  return (
    <div className={list_style.list_container}>
      <p className={list_style.type}>{type} - </p>
      {item}
    </div>
  );
};

export default CarouselList;
