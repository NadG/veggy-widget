import React from 'react';
import Headline from 'components/Atoms/Headline/Headline';
import { getFormattedText } from 'utils/dataDisplayMethods';
import InlineIcons from 'components/Atoms/Icons/InlineIcons';
import Direction from 'components/Atoms/Direction/Direction';
import rh from './RecipeHeader.module.scss';

interface Props {
  list?: string[];
  title: string;
}

const RecipeHeader: React.FC<Props> = ({ list, title }) => {
  return (
    <div className={rh.container}>
      <Direction directionType="row" justifyContent="sb">
        <div className={rh.headline}>
          <Headline text={getFormattedText(title)} size="lg" />
        </div>
        {list && (
          <div className={rh.list}>
            {list.map((e: string, i: number) => (
              <InlineIcons key={i} name={e} size={25} />
            ))}
          </div>
        )}
      </Direction>
    </div>
  );
};

export default RecipeHeader;
