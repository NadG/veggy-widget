import React from 'react';
import InlineIcons from 'components/Atoms/Icons/InlineIcons';
import recipes_style from 'components/Templates/Recipe/recipe.module.scss';

interface Props {
  generics: object;
  time: string;
}

const RecipeGenerics: React.FC<Props> = ({ generics, time }) => {
  let genericsArr = [];

  for (const [key, value] of Object.entries(generics)) {
    value && genericsArr.push(key);
  }

  return (
    <div className={recipes_style.icons_container}>
      <div className={recipes_style.icon_wrapper}>
        <InlineIcons name="time" size={30} />
        <p>{time}</p>
      </div>
      {genericsArr.map((
        e,
        i, // this should be its own component
      ) => (
        <div key={i} className={recipes_style.icon_wrapper}>
          <InlineIcons name={e} size={30} />
          <p>{e}</p>
        </div>
      ))}
    </div>
  );
};

export default RecipeGenerics;
