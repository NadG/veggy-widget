import React from 'react';
import item_style from 'components/Molecules/Item/CarouselItem.module.scss';
import { NavLink } from 'react-router-dom';
import InlineIcons from 'components/Atoms/Icons/InlineIcons';

interface ItemProps {
  item: string;
}

const CarouselItem: React.FC<ItemProps> = ({ item }) => {
  const handleViewReceipe = (option: string) => {
    console.log(option);
  };

  return (
    <NavLink to={'/receipes/' + item} className={item_style.link}>
      <div className={item_style.item} onClick={() => handleViewReceipe(item)}>
        <InlineIcons name={item} size={25} />
        <p>{item}</p>
      </div>
    </NavLink>
  );
};

export default CarouselItem;
