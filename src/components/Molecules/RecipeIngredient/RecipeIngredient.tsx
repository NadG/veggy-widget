import React from 'react';
import { getFormattedText } from 'utils/dataDisplayMethods';
import i from 'components/Molecules/RecipeIngredient/RecipeIngredient.module.scss';
import { ReactComponent as Daisy } from 'assets/icons/misc/daisy.svg';

interface Props {
  name: string;
  quantity: string;
}

const RecipeIngredient: React.FC<Props> = ({ ...props }) => {
  return (
    <div className={i.container}>
      <Daisy className={i.dot} />
      <p className={i.name}>{getFormattedText(props.name)}:</p>
      <span>{props.quantity}</span>
    </div>
  );
};

export default RecipeIngredient;
