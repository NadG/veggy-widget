import * as React from 'react';
import { CSSTransition } from 'react-transition-group';
import ModalContainer from './ModalContainer';
import styles from './Modal.module.scss';
import fade from './fade.module.scss';
import slide from './slide.module.scss';
import { ModalState } from 'hooks/useModal';

interface Props {
  title: string;
  state: ModalState;
  onVisible?: () => Promise<void>;
}

const Modal: React.FC<Props> = ({ title, state, children, onVisible }) => {
  React.useEffect(() => {
    if (state.isVisible && !!onVisible) {
      onVisible();
    }
  }, [state.isVisible, onVisible]);

  return (
    <ModalContainer>
      <CSSTransition
        in={state.isVisible}
        timeout={150}
        classNames={{ ...fade }}
        mountOnEnter
        unmountOnExit
      >
        <div className={styles.container}>
          <div className={styles.backdrop} />
          <div className={styles.modal}>
            <CSSTransition
              in={state.isVisible}
              timeout={150}
              classNames={{ ...slide }}
              appear
            >
              <div className={styles.dialog}>
                <div className={styles.header}>
                  <div className={styles.title}>{title}</div>
                  <button
                    className={styles.close}
                    type="button"
                    onClick={state.close}
                  >
                    Close
                  </button>
                </div>
                <div className={styles.body}>{children}</div>
              </div>
            </CSSTransition>
          </div>
        </div>
      </CSSTransition>
    </ModalContainer>
  );
};

export default Modal;
