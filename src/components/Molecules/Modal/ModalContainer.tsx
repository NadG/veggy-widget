import * as React from 'react';
import ReactDOM from 'react-dom';

const modalRoot = document.getElementById('react-modal-root')!;

class ModalContainer extends React.Component<{}> {
  el: HTMLDivElement;

  constructor(props: {}) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    if (modalRoot) {
      modalRoot.appendChild(this.el);
    }
  }

  componentWillUnmount() {
    if (modalRoot) {
      modalRoot.removeChild(this.el);
    }
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el);
  }
}

export default ModalContainer;
