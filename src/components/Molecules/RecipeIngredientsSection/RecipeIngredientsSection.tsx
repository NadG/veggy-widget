import React from 'react';
import r from 'components/Molecules/RecipeIngredientsSection/RecipeIngredientsSection.module.scss';
import Subtitle from 'components/Atoms/Subtitle/Subtitle';
import RecipeIngredient from 'components/Molecules/RecipeIngredient/RecipeIngredient';
import { RecipeIngredients } from 'types/recipe';

interface Props {
  ingredients: RecipeIngredients[];
}

const RecipeIngredientsSection: React.FC<Props> = ({ ...props }) => {
  return (
    <>
      <Subtitle words="Ingredients" />
      <div className={r.list}>
        {props.ingredients.map((ingredient: any, i: number) => (
          <RecipeIngredient
            name={ingredient.name}
            quantity={ingredient.quantity}
            key={i}
          />
        ))}
      </div>
    </>
  );
};

export default RecipeIngredientsSection;
