import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import VeggieWidgetContainer from 'components/Pages/VeggieCarousel/VeggieCarousel';
import RecipesList from 'components/Pages/RecipesList/RecipesList';
import RecipeForm from 'components/Templates/RecipeForm/RecipeForm';
import m from 'components/Pages/Main/Main.module.scss';
class Main extends Component {
  render() {
    return (
      <div className={m.main}>
        <Switch>
          <Route exact path="/" component={VeggieWidgetContainer} />
          <Route path="/receipes/:ingredient?" component={RecipesList} />
          <Route exact path="/new-recipe/" component={RecipeForm} />
        </Switch>
      </div>
    );
  }
}

export default Main;
