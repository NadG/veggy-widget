import React from 'react';
import 'assets/scss/icons.scss';
import cn from 'classnames';
import dir from './Direction.module.scss';

interface ItemProps {
  directionType: string;
  wrap?: boolean;
  justifyContent?: 'sb' | 'around' | 'center';
  alignItems?: 'center';
}

const Direction: React.FC<ItemProps> = ({
  directionType,
  children,
  wrap,
  justifyContent,
  alignItems,
}) => {
  return (
    <div
      style={{ height: '100%' }}
      className={cn({
        [dir.row]: directionType === 'row',
        [dir.column]: directionType === 'column',

        [dir.jc_sb]: justifyContent === 'sb',
        [dir.jc_a]: justifyContent === 'around',
        [dir.jc_c]: justifyContent === 'center',

        [dir.ai_c]: alignItems === 'center',

        [dir.wrap]: wrap,
      })}
    >
      {children}
    </div>
  );
};

export default Direction;
