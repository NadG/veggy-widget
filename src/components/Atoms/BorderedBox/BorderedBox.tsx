import React from 'react';
import ib from 'components/Atoms/BorderedBox/BorderedBox.module.scss';

const BorderedBox: React.FC = ({ children }) => {
  return <div className={ib.container}>{children}</div>;
};

export default BorderedBox;
