import React from 'react';

interface Icon {
  name: string;
  size: number;
  category?: string;
}
/* Icon classes are currently in a global scss file.
TODO: - make icon scss a module for this component.
 - make it responsive */
const Icons: React.FC<Icon> = ({ size, name }) => {
  const style = {
    display: 'block',
    backgroundRepeat: 'no-repeat',
    height: `${size}px`,
    width: `${size}px`,
    backgroundSize: `${size}px`,
  };

  return <span style={style} className={`icon_${name}`} />;
};

export default Icons;
