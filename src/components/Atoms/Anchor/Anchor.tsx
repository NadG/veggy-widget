import React from 'react';
import l from 'components/Atoms/Anchor/Anchor.module.scss';
import Flex from 'components/Atoms/Flex/Flex';

interface Props {
  link: string;
  linkName: string;
}
const Anchor: React.FC<Props> = ({ children, linkName, link }) => {
  return (
    <a href={link} className={l.link} target="_blank" rel="noopener noreferrer">
      {children ? (
        <Flex row aiCenter>
          <p>{linkName}</p>
          {children}
        </Flex>
      ) : (
        <p>{linkName}</p>
      )}
    </a>
  );
};

export default Anchor;
