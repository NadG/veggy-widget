import React from 'react';
import Option from './Option';
//import { useField } from 'formik';

interface SelectProps {
  name: string;
  data: string[];
}

const Select: React.FC<SelectProps> = ({ name, ...props }) => {
  const [openList, setOpenList] = React.useState(false);
  //const [field, meta, helpers] = useField(name);

  return (
    <div>
      <div
        style={{ background: 'green', height: '50px' }}
        onClick={() => setOpenList(!openList)}
      >
        Select main ingredient
      </div>
      {openList
        ? props.data.map((option, index) => {
            return <Option key={index} options={option} />;
          })
        : null}
    </div>
  );
};
export default Select;
