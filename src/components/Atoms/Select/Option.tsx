import React from 'react';

interface OptionProps {
  options: any;
}

const Option: React.FC<OptionProps> = ({ options, children }) => {
  return (
    <div style={{ borderBottom: '2px solid green' }}>
      {options}
      {children}
    </div>
  );
};
export default Option;
