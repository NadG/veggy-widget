import React from 'react';
import h from './Headline.module.scss';
import classnames from 'classnames';

interface HeadlineProps {
  text: string;
  size?: string;
  bold?: boolean;
}

const Headline: React.FC<HeadlineProps> = ({ size, text, bold }) => {
  return (
    <p
      className={classnames(h.title, {
        [h.xsmall]: size === 'xs',
        [h.small]: size === 'sm',
        [h.default]: size === 'default',
        [h.medium]: size === 'md',
        [h.large]: size === 'lg',
        [h.xlarge]: size === 'xl',
        [h.xxlarge]: size === 'xxl',
      })}
    >
      {text}
    </p>
  );
};

export default Headline;
