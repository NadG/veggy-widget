import React from 'react';
import s from 'components/Atoms/Subtitle/Subtitle.module.scss';

interface SubtitleProps {
  words: string;
}

const Subtitle: React.FC<SubtitleProps> = ({ words }) => {
  return <p className={s.subtitle}>{words}</p>;
};

export default Subtitle;
