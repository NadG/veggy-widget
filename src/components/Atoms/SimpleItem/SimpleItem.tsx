import React from 'react';
import si from 'components/Atoms/SimpleItem/SimpleItem.module.scss';

interface SimpleItemProps {
  element: string;
}

const SimpleItem: React.FC<SimpleItemProps> = ({ element }) => (
  <span className={si.spacing}>{element}</span>
);

export default SimpleItem;
