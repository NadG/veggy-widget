import React from 'react';
import { ReactComponent as Leaf } from 'assets/icons/recipe-type/leaf.svg';
import header from 'components/Atoms/Header/Header.module.scss';
import { NavLink } from 'react-router-dom';
import Flex from 'components/Atoms/Flex/Flex';

const Header = () => {
  return (
    <header className={header.container}>
      <Flex row aiCenter jcBetween>
        <NavLink to="/" className={header.logo}>
          <h1>Basilico </h1>
          <Leaf className={header.Leaf} />
          <p className={header.subtitle}>
            {' '}
            - Italian recipes for italian food lovers
          </p>
        </NavLink>
      </Flex>

      {/*<NavLink to={'new-recipe'}>Add new recipe</NavLink>*/}
    </header>
  );
};

export default Header;
