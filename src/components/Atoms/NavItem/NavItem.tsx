import * as React from 'react';
import { NavLink } from 'react-router-dom';
import ni from './NavItem.module.scss';

interface Props {
  to: string;
}

const NavItem: React.FunctionComponent<Props> = ({ to, children }) => (
  <NavLink to={to} className={ni.link}>
    {children}
  </NavLink>
);

export default NavItem;
