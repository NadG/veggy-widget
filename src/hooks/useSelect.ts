import { useState } from 'react';

export interface SelectState {
  showOptions: boolean;
  toggle: () => void;
}

const useSelect = (): SelectState => {
  const [showOptions, setShowOptions] = useState(false);

  function toggle() {
    setShowOptions(!showOptions);
  }

  return {
    showOptions,
    toggle,
  };
};

export default useSelect;
