import { useLayoutEffect, useState } from 'react';

function useWindowSize() {
  const [size, setSize] = useState([0, 0]);

  useLayoutEffect(() => {
    window.addEventListener('resize', () =>
      setSize([window.innerWidth, window.innerHeight]),
    );
    return () =>
      window.removeEventListener('resize', () =>
        setSize([window.innerWidth, window.innerHeight]),
      );
  }, []);
  return size;
}

export default useWindowSize;
