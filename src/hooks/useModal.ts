import { useState } from 'react';

export interface ModalState {
  isVisible: boolean;
  toggle: () => void;
  open: () => void;
  close: () => void;
}

const useModal = (): ModalState => {
  const [isVisible, setIsVisible] = useState(false);

  function toggle() {
    setIsVisible(!isVisible);
  }

  function open() {
    setIsVisible(true);
  }

  function close() {
    setIsVisible(false);
  }

  return {
    isVisible,
    toggle,
    open,
    close,
  };
};

export default useModal;
