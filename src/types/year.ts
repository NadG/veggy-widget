import { Month } from 'types/months';

export interface Year {
  months: Month[];
}
